<%@page import="java.util.concurrent.atomic.AtomicInteger"%>
<%@page import="java.util.HashMap"%>
<%@page import="com.firstrain.business.pojo.OwnerProfileJson"%>
<%@page import="com.firstrain.business.util.DefaultEnums.OwnedByType"%>
<%@page import="com.firstrain.business.pojo.input.AddUpdateProfileBean"%>
<%@page import="com.firstrain.utils.BusinessJSONUtility"%>
<%@page import="com.firstrain.business.domain.UserProfile"%>
<%@page import="com.firstrain.business.domain.User"%>
<%@page import="com.firstrain.business.service.impl.OwnerProfileServiceImpl"%>
<%@page import="org.springframework.web.context.support.WebApplicationContextUtils"%>
<%@page import="org.springframework.web.context.WebApplicationContext"%>
<%@page import="com.firstrain.business.service.impl.UserBriefServiceImpl"%>
<%@page import="org.apache.log4j.Logger"%>
<%@page import="com.firstrain.utils.object.PerfMonitor"%>
<%@page import="com.firstrain.springcore.HttpClientService1"%>
<%@page import="java.util.Map"%>
<%@ page language="java" session="false" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Orion User Migration</title>
</head>
<body>
<%!private static final Logger LOG = Logger.getLogger("userProfileMetaMigration");

	private static UserBriefServiceImpl userBriefService;
	private static OwnerProfileServiceImpl ownerProfileServiceImpl;
	private static boolean isAlreadyStarted = false;

	private static void getBeansFromWebContext(ServletContext context) {
		WebApplicationContext webContext = WebApplicationContextUtils.getWebApplicationContext(context);
		userBriefService = webContext.getBean(UserBriefServiceImpl.class);
		ownerProfileServiceImpl = webContext.getBean(OwnerProfileServiceImpl.class);
	}
%>
	<%
		if (isAlreadyStarted) {
	%>
	<h2>Process is already running in background, Please monitor logs
		for its completion and don't refresh.</h2>
	<%
		} else {

			PerfMonitor.startRequest("", "");
	%>
	<h2>Orion UserProfile Migration is in progress</h2>
	<%
	isAlreadyStarted = true;
	long st = System.nanoTime();
	String USER_ID_CSV = "";
	
	try{
		getBeansFromWebContext(application);
		out.println("Execution of UserProfileMetaMigration jsp start");
		out.println("<br>");
		out.flush();
		if(USER_ID_CSV != null) {
			int count = 0;
			AtomicInteger countSuccess = new AtomicInteger();
			AtomicInteger countIgnore = new AtomicInteger();
			String userIds = "";
			StringBuilder ignoreUserIds = new StringBuilder("");
			for(String id : USER_ID_CSV.split(",")) {
				if(!(id = id.trim()).isEmpty()) {
					AtomicInteger countFailure = new AtomicInteger();
					StringBuilder failedUserIds = new StringBuilder("");
					long userId = Long.parseLong(id);
					User user = userBriefService.getUserById(userId);
					if(user != null) {
						UserProfile userProfile = userBriefService.getUserProfile(userId, true);
						if(userProfile != null && userProfile.getRole() != null) {
							try {
								Map<String, String> metaData = null;
								if(userProfile.getMetadata() != null) {
									metaData = BusinessJSONUtility.deserialize(userProfile.getMetadata(), Map.class);
								} else {
									metaData = new HashMap<String, String>();
								}
								metaData.put("beaconCount", "1");
								OwnerProfileJson profileJson = new OwnerProfileJson();
								profileJson.setMetadata(BusinessJSONUtility.serialize(metaData));
				 				AddUpdateProfileBean updateProfileInputBean = new AddUpdateProfileBean();
				 				updateProfileInputBean.setOwnerId(userId);
				 				updateProfileInputBean.setOwnerType(OwnedByType.USER);
				 				updateProfileInputBean.setProfileJson(profileJson);
				 				updateProfileInputBean.setRoleType(userProfile.getRole());
				 				updateProfileInputBean.setPersonaType(userProfile.getPersona());
				 				updateProfileInputBean.setRoleLabel(userProfile.getRoleCaption());
				 				
				 				ownerProfileServiceImpl.addOrUpdateUserProfile(updateProfileInputBean);
				 				countSuccess.incrementAndGet();
				 				out.println("Metadata updated for user = " + userId);
				 				out.println("<br>");
				 				
				 				count = count + countFailure.get();
								userIds = userIds + failedUserIds.toString();
								LOG.info("Processed successfully " + countSuccess.get() + " number of user ");
								if(countFailure.get() > 0) {
									LOG.error("Process Failed for " + countFailure.get() + " users , their userIds : " + failedUserIds.toString());
									out.println("Process Failed for " + countFailure.get() + " users , their userIds : " + failedUserIds.toString()+"<BR />");
								}
								out.println("Processed successfully " + countSuccess.get() + " number of user <BR />");
								if(countFailure.get() > 0) {
									out.println("Failure " + countFailure.get() + " number of user , their userIds : " + failedUserIds.toString()+"<BR />");
								}
							} catch(Exception e) {
								LOG.error("user " + userId + " does not migrated because of error: " + e.getMessage());
								countFailure.incrementAndGet();
								failedUserIds.append(userId + ", ");
							}
						} else {
							ignoreUserIds.append(userId + ", ");
							countIgnore.incrementAndGet();
		 					out.println("role or persona or role label was null in profile. Skipping to update profile for this User " + userId);
		 					out.println("<br>");	
						}
					} else {
						countIgnore.incrementAndGet();
						ignoreUserIds.append(userId + ", ");
						out.println("userId = " + userId + " is not present");
						out.println("<br>");
					}
					out.flush();
				}
			}
			out.println("<BR />Total Failure: " + count + " their Ids " + userIds+"<BR />");
			out.println("Total Ignore: " + countIgnore.get() + " their Ids " + ignoreUserIds.toString()+"<BR />");
			out.println("Total Success: " + countSuccess.get() );
			LOG.info("Total Failure: " + count + " their Ids " + userIds);
			LOG.info("Total Ignore: " + countIgnore.get() + " their Ids " + ignoreUserIds.toString());
			LOG.info("Total Success: " + countSuccess.get() );
		} else {
			out.println("USER_ID_CSV is null");
			out.println("<br>");
			out.flush();
		}
	} finally {
		isAlreadyStarted = false;
	}
	%>
	<h2>
		Completed in
		<%=((System.nanoTime() - st) / 1000000)%>
		ms.
	</h2>
	<%
		}
	
	%>